from pyinotify import WatchManager, Notifier, EventsCodes, ProcessEvent, ThreadedNotifier
import pyinotify
import socket

wm = WatchManager()

class P(ProcessEvent):
    def process_IN_CREATE(self, event):
        print "create"

    def process_IN_ACCESS(self, event):
        print "access"

    def process_IN_DELETE(self, event):
        print "del"

mask = pyinotify.IN_CREATE | pyinotify.IN_DELETE

wd = wm.add_watch("/boot", mask, rec=True)

notifier = Notifier(wm, P())

if __name__ == "__main__":

    print "Watching"
    while True:
        try:
            notifier.process_events()
            if notifier.check_events():
                notifier.read_events()
            print "OKOk"
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.connect(('localhost', 8000))
            sock.sendall("sync")
            sock.close()

        except KeyboardInterrupt:
            notifier.stop()
            break





